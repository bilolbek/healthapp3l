package com.example.healthapp3l

import android.media.MediaPlayer
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.widget.ImageView
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import java.util.*


class MainActivity : AppCompatActivity() {

    var mProgressBar: ProgressBar? = null
    var textView: TextView? = null
    var exercise_pic: ImageView? = null
    var i = 110
    var mediaPlayer: MediaPlayer? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val actionbar = supportActionBar
        actionbar!!.title = "Health app"
        actionbar.setDisplayHomeAsUpEnabled(true)
        actionbar.setDisplayHomeAsUpEnabled(true)
        setUp()


        val first = mProgressBar?.let {
            textView?.let { it1 ->
                exercise_pic?.let { it2 ->
                    ExerciseModel(
                        30000, 1000, it, it1,
                        it2, R.drawable.ic_push_up
                    )
                }
            }
        }
        val firstRest = mProgressBar?.let {
            textView?.let { it1 ->
                exercise_pic?.let { it2 ->
                    ExerciseModel(
                        10000, 1000, it, it1,
                        it2, null
                    )
                }
            }
        }
        val second = mProgressBar?.let {
            textView?.let { it1 ->
                exercise_pic?.let { it2 ->
                    ExerciseModel(
                        30000, 1000, it, it1,
                        it2, R.drawable.ic_jumping_jacks
                    )
                }
            }
        }
        val secondRest = mProgressBar?.let {
            textView?.let { it1 ->
                exercise_pic?.let { it2 ->
                    ExerciseModel(
                        10000, 1000, it, it1,
                        it2, null
                    )
                }
            }
        }

        val third = mProgressBar?.let {
            textView?.let { it1 ->
                exercise_pic?.let { it2 ->
                    ExerciseModel(
                        30000, 1000, it, it1,
                        it2, R.drawable.ic_abdominal_crunch
                    )
                }
            }
        }
        val player = object : CountDownTimer(110000, 1000) {
            override fun onTick(millisUntilFinished: Long) {

                if (millisUntilFinished <= 80000L && millisUntilFinished >= 70000L) {
                    Log.d("is in 1", millisUntilFinished.toString())
                    if (!mediaPlayer!!.isPlaying)
                        mediaPlayer!!.start()
                } else if (millisUntilFinished <= 40000L && millisUntilFinished >= 30000L) {
                    Log.d("is in 2", millisUntilFinished.toString())
                    if (!mediaPlayer!!.isPlaying)
                        mediaPlayer!!.start()
                } else {
                    if (mediaPlayer!!.isPlaying)
                        mediaPlayer!!.pause()
                }


            }

            override fun onFinish() {
                mediaPlayer!!.release()
            }

        }

        setUpActivities(Arrays.asList(first, firstRest, second, secondRest, third))
        first!!.startWithImage()
        mediaPlayer = MediaPlayer.create(this, R.raw.press_start)
        mediaPlayer!!.isLooping = true
        player.start()
    }


    fun setUpActivities(activities: List<ExerciseModel?>) {
        var count = 0;
        while (count != activities.size - 1) {
            activities.get(count)!!.nextExercise = activities.get(count + 1)
            count++;
        }
    }


    fun setUp() {
        textView = findViewById(R.id.text_view_progress)
        mProgressBar = findViewById(R.id.progress_bar)
        exercise_pic = findViewById(R.id.imageID)
        mProgressBar!!.setProgress(i)
        textView!!.text = (i / 10).toString(); }
}